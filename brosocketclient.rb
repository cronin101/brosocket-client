#!/usr/bin/ruby

require "socket"
require "rubygems"
#coloured terminal output
require "colored"

while true
  begin
    #IP Address of Suicune
    server = TCPSocket.new('46.102.244.231',7777)
  rescue
    puts "Connection with server could not be made, trying again in 10 seconds.".red
    sleep 10
    next
  end
  #Handshake message
  message = "Hello, my name is #{`uname -n`.chop}!"
  puts "> #{message}".blue
  server.puts message

  #Get reply
  reply = server.gets
  puts "< #{reply}".green

  #Password Auth
  server.puts `cat /home/cronin/Brogramming/BroSocket/passtoken.txt`

  #Listen for notifications
  while reply = server.gets
    #Keepalive tests
    if reply.chop == "Hi."
      server.puts "Hi."
      next
    end
    puts "< #{reply}".green
    #Data to be displayed as a notification
    if reply.include?("#SHOW:")
      `notify-send '#{reply.split("SHOW:")[1].gsub("'","")}' -t 6000&`
    end
    server.puts "Thanks!"
  end

  puts "Connection lost.".red
  server.close
end
