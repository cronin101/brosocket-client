#!/usr/bin/ruby

require "socket"

#Socket listening on port 7777
sock = TCPServer.new(7777)

#For each connection
while laptop = sock.accept
  
  #Client details
  puts "Connection initiated by #{laptop.peeraddr.inspect}"
  
  #For input
  while s = laptop.gets

    #lame sanity checks
    if !s.include?("my name is ") or !(s.chop[-1] == "!")
      #Show insane message
      puts "< #{s}"
      puts "Input failed sanity check."
      #Don't talk to strangers
      message = "I have no idea who you are #{laptop.peeraddr[2]}... Please go away!"
      puts "> #{message}"
      laptop.puts message
      next
    end

    #Show message
    puts "< #{s}"
    name = s.split("my name is ")[1].split("!")[0]
    next if !name

    #Reply to message
    if name == "Piplup"
      message = "Hello #{name}, I am Suicune!"
      laptop.puts message
      puts "> #{message}"

      #Second layer, password authentication
      token = laptop.gets
      if !token or (token != `cat passtoken.txt`)
        puts "Incorrect password token. Aborted."
        next
      end
      puts "Password authentication succeeded!"
      

      #Loop forever relaying information
      messagespath = "/home/cronin/messagesforlaptop"
      connection = true
      while connection
      
      #lazy way to limit rate
      sleep 1

        #Check connection still exists
        begin
          laptop.puts "Hi."
          reply = laptop.gets
          if !reply or !(reply.chop == "Hi.")
            puts "Connection Error."
            connection = false
            next
          end
        rescue
          next
        end
        
        #If relay buffer exists, relay it
        if FileTest.exists?(messagespath)
          puts "I have a message for #{name}!"
          file = File.new(messagespath, "r")
          while line = file.gets
            laptop.puts line
            reply = laptop.gets
            if !reply or !(reply.chop == "Thanks!")
              puts "Connection Error."
              connection = false
              return
            end

          end
          puts "Message received!"
          #clear buffer when complete
          `rm /home/cronin/messagesforlaptop`
        end
      end
    else
      #Be polite to others who follow the protocol
      message = "You aren't Cronin's laptop, but hello anyway #{name}!"
      laptop.puts message
      puts "> #{message}"
    end
  end
  puts "Connection terminated."
end
